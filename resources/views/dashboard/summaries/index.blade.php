@extends('layouts.dashboard')

@section('content')
    <div class="px-10 py-5 md:px-20 md:py-10 transition-all">
        <div class="flex">
            <div class="w-1/2 flex items-center">
                <p class="text-3xl font-extrabold text-gray-900 dark:text-white">Summaries</p>
            </div>
            <div class="w-1/2 text-right">

            </div>
        </div>
    </div>
    <div class="px-10 md:px-20 transition-all">

        @if(count($summaries) == 0)
            <div class="w-100 text-center flex items-center justify-center flex-column">
                <p class="font-medium text-lg">{{ __('You dont have any summaries yet, it will take time') }}</p>
                <img src="{{ asset('images/empty_1.svg') }}" class="w-25 my-24" alt="">
            </div>
        @else
            <div class="flex flex-col">
                <div class="overflow-x-auto sm:-mx-6 lg:-mx-8 ">
                    <div class="py-2 align-middle inline-block w-full sm:px-6 lg:px-8">
                        {{ $summaries->links() }}
                        <div class="overflow-hidden border-b border-gray-200 dark:border-truegray-800 sm:rounded-lg shadow-md rounded-lg">
                            <table class="min-w-full divide-y divide-gray-200 dark:divide-truegray-800">
                                <thead class="bg-gray-50 dark:bg-truegray-800">
                                <tr>
                                    <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider dark:text-gray-300">
                                        Product configured
                                    </th>
                                    <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider dark:text-gray-300">
                                        Amount of options
                                    </th>
                                    <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider dark:text-gray-300">
                                        Code
                                    </th>
                                    <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider dark:text-gray-300">
                                        Total
                                    </th>
                                    <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider hidden dark:text-gray-300">
                                        Status
                                    </th>
                                    <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider dark:text-gray-300">
                                        Created at
                                    </th>
                                    <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider dark:text-gray-300">
                                        Updated at
                                    </th>
                                </tr>
                                </thead>
                                <tbody class="bg-white dark:bg-truegray-900 divide-y divide-gray-200 dark:divide-truegray-800">
                                @foreach($summaries as $summary)
                                    <tr>
                                        <td class="px-6 py-3 whitespace-nowrap text-sm dark:text-white">
                                            {{ $summary->products[0]['name'] }}

                                        </td>
                                        <td class="px-6 py-3 whitespace-nowrap text-sm dark:text-white">
                                            {{ $summary->options->count() }}
                                        </td>
                                        <td class="px-6 py-3 whitespace-nowrap text-sm dark:text-white">
                                            <a class="text-indigo-500" href="/view_summary/{{$summary->code}}">{{ $summary->code }}</a>
                                        </td>
                                        <td class="px-6 py-3 whitespace-nowrap text-sm dark:text-white">
                                            € {{ $summary->total }}
                                        </td>
                                        <td class="px-6 py-3 whitespace-nowrap text-sm hidden dark:text-white">
                                            <div class="flex">
                                                <div class="bg-yellow-200 text-yellow-800 text-sm rounded-lg p-1 px-3 font-medium text-center w-auto">Open</div>
                                            </div>
                                            <div class="flex">
                                                <div class="bg-orange-200 text-orange-800 text-sm rounded-lg p-1 px-3 font-medium text-center w-auto">Pending</div>
                                            </div>
                                             <div class="flex">
                                                 <div class="bg-green-200 text-green-800 text-sm rounded-lg p-1 px-3 font-medium text-center w-auto">Paid</div>
                                             </div>
                                            <div class="flex">
                                                <div class="bg-red-200 text-red-800 text-sm rounded-lg p-1 px-3 font-medium text-center w-auto">Failed</div>
                                            </div>
                                            <div class="flex">
                                                <div class="bg-gray-200 text-gray-800 text-sm rounded-lg p-1 px-3 font-medium text-center w-auto">Cancelled</div>
                                            </div>
                                            <div class="flex">
                                                <div class="bg-gray-200 text-gray-800 text-sm rounded-lg p-1 px-3 font-medium text-center w-auto">Expired</div>
                                            </div>

                                        </td>
                                        <td class="px-6 py-3 whitespace-nowrap text-sm dark:text-white">
                                            {{ $summary->created_at->toFormattedDateString() }}
                                        </td>
                                        <td class="px-6 py-3 whitespace-nowrap text-sm dark:text-white">
                                            {{ $summary->updated_at->toFormattedDateString() }}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        {{ $summaries->links() }}
                    </div>
                </div>
            </div>
        @endif
    </div>
@endsection
