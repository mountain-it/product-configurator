<?php

namespace App\Http\Controllers;

use App\Download;
use App\Models\Product;
use App\Models\Summary;
use Barryvdh\DomPDF\Facade as PDF;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SummaryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $summaries = Summary::orderBy('created_at', 'desc')->paginate(20);

        return view('dashboard.summaries.index', compact('summaries'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // In the request the ID of the parent product has been given, we need to search it in the datbaase to know more.
        $parent = Product::find($request['parent']['id']);

        // We'll set the sum of the order to the parent price for now.
        $sum = $parent->price;

        // We'll create a new summary now and fill all the fields that can be filled without knowing something about the options.
        // This needs to be replaced by Order
        $summary = Summary::create([
            'code' => strtoupper(bin2hex(openssl_random_pseudo_bytes(4))),
            'total' => 0,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        // This will fill the summary_products table. We created the summary beforehand and now connect the parent to it.
        // Which allows us to do $summary->products
        $summary->products()->attach($parent->id);

        // Now we go to all the options that are giving in the request
        foreach ($request['options'] as $option) {
            foreach($option[0]['options'] as $product) {
                // And find each product individually so we know more about it.
                $product = Product::find($product['id']);
                // Here we fill the summary_options table with the ID's, remember this is in a foreach loop so this code
                // Will be executed for every option in the array
                $product->opties()->attach($summary->id);
                // And then we will up the sum with the price of the product
                $sum += $product['price'];
            }
        }

        // Because we created the summary beforehand we only need to update the fields that we didn't set at the start.
        // In this case that would be the total but could also be payment status or anything really.
        Summary::where('id', $summary->id)->update(['total' => $sum]);

        // And just return a response with a status or just blank to know it went through
        return response()->json();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $summary = Summary::where('code', '=', $id)->firstOrFail();

        return view('dashboard.summaries.show', compact('summary'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getAllSummaries()
    {
        return response()->json(Summary::with('products')->get());
    }

    public function generatePDF(Request $request)
    {
//        dd($request);
        $pdf = PDF::loadView('pdfs.summary', ['active' => $request->activeProduct, 'options' => $request->chosenOptions]);
//        $dompdf->stream("dompdf_out.pdf", array("Attachment" => false));
//        $download = Summary::create([
//            'user_id' => Auth::user()->id,
//            'filename' => 'summary.pdf'
//        ]);

        return $pdf->download('summary.pdf');
    }

    public function getMostPopulairProduct()
    {
//        dd(Summary::groupBy('created_at')->sum('total'));

        return response()->json(Product::withCount('summaries')->orderBy('summaries_count', 'desc')->first());
    }

    public function getSummaryStatistics() {
        return response()->json(DB::table('summaries')
            ->select(DB::raw('DATE(created_at) as date'), DB::raw('sum(total) as sum'))
            ->groupBy('date')->pluck('sum','date'));
    }
}
