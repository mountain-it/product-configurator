# About this project

The product configurator is a project which allows companies to add configurable products in a standalone system. The purpose of this project is to make a configurator which is applicable to all different situations. This project allows the user to create, edit, delete products, manufacturers, steps etc.

### Build with

In the following list all major frameworks/libraries the project are shown.

* [Laravel 8](https://laravel.com/) (Needs to be updated to the latest version)
* [Vue.js](https://vuejs.org/)
* [TailwindCSS](https://tailwindcss.com/)
* [Heroicons](https://heroicons.com/)

# Getting started 🚀

To install the product configurator, please visit the [Laravel website](https://laravel.com/) to install it onto your system. Also make sure you install [Composer](https://getcomposer.org/) and [Node.js](https://nodejs.org/en/)


First you'll need to clone the project onto your local environment. To do so you can clone the repository using:

```zsh
git clone https://rainierlaan@bitbucket.org/mountain-it/product-configurator.git
```

Once you have the project installed you'll need to install all the dependicies and packages. You can do this by doing the following:

```
composer install
```
```
npm install
```

These two will install all the neccesary packages from Composer and Node.js

In the project there should be a `.env.example` file, you should rename this file to `.env` and change the database settings to suit your system. If you do not have a `.env` file you can simply create one and put in the following:

```
APP_NAME=Laravel
APP_ENV=local
APP_KEY=
APP_DEBUG=true
APP_URL=http://localhost

LOG_CHANNEL=stack
LOG_LEVEL=debug

DB_CONNECTION=mysql
DB_HOST=localhost
DB_PORT=3306
DB_DATABASE=configurator
DB_USERNAME=user
DB_PASSWORD=password

BROADCAST_DRIVER=log
CACHE_DRIVER=file
FILESYSTEM_DRIVER=local
QUEUE_CONNECTION=sync
SESSION_DRIVER=file
SESSION_LIFETIME=120

MEMCACHED_HOST=127.0.0.1

REDIS_HOST=127.0.0.1
REDIS_PASSWORD=null
REDIS_PORT=6379

MAIL_MAILER=smtp
MAIL_HOST=mailhog
MAIL_PORT=1025
MAIL_USERNAME=null
MAIL_PASSWORD=null
MAIL_ENCRYPTION=null
MAIL_FROM_ADDRESS=null
MAIL_FROM_NAME="${APP_NAME}"
```

Change all the variables to suit your local environment.

After you've done everything correctly you should be able to migrate all the data to the database and start programming. To do so you'll need to run the following command:

```
php artisan migrate
```

This will create all the tables and relations of the configurator. If you wish to have the project with sample data you'll need to add `-seed` behind the command to run the Laravel seeders.

If everything went correctly you should have a fully functional working configurator.

# For mac users that are using valet

If you are using a Mac and your project runs using [Laravel valet](https://laravel.com/docs/8.x/valet) you can easily make virtual domain to show the project in the browser.

Create a folder called `Sites` and run `valet park` to register that directory as your working directory. Next you'll need to navigate to your project folder and run `valet link` this will create a symbolic link and create a `.test` domain. If your folder is called configurator, your project will be available on `configurator.test`. Last you'll need to create a SSL certificate for this test domain. You can easily do this by doing `laravel secure`. If everything went correctly, you should see the configurator at your chosen domain.

# All related files

In the Google Drive link below is a ZIP file of all the related documentation for this project. It contains

* Plan of action
* Functional design
* Techincal design
* An explanation for the choices made and how they're executed

You can use these files as a reference point if you don't quite understand how things work. These files will clarify it for you.

https://drive.google.com/file/d/1MEBL4NrPJvR2Bm06Q54_avtPGVO1HBin/view?usp=sharing
